# DLIOpy

Python Implementation of [Direct Lidar Inertial Odometry](https://ieeexplore.ieee.org/document/10160508)

## Instructions

### Dependencies
The following has been verified to be compatible:

- Ubuntu 22.04
- ROS2 Humble (`rclpy`, `std_msgs`, `sensor_msgs`, `geometry_msgs`, `nav_msgs`)
- numpy

### Compiling

Inside your ros2 workspace:
```sh
mkdir src && cd src
git clone git@gitlab.com:neu-autonomy/dliopy.git
colcon build --packages-select dliopy
```

### Running

```sh
source install/local_setup.bash
ros2 launch dliopy dliopy_launch.py \
    rviz:={true, false} \
    pointcloud_topic:=/your/lidar/topic \
    imu_topic:=/your/imu/topic
```