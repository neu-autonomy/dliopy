import torch
import numpy as np
import threading
import pcl
import struct

import copy

from sensor_msgs.msg import PointCloud2, PointField
from std_msgs.msg import Header


# ============================
# Downsampling Helper Function
# ============================


def gridSampling(pcd: torch.tensor, resolution=0.02):

    grid = torch.floor(pcd/resolution)
    center = (grid+0.5)*resolution
    dist = ((pcd-center)**2).sum(dim=1)
    dist = dist/dist.max()*0.7

    v_size = np.ceil(1/resolution)
    grid_idx = grid[:, 0] + grid[:, 1] * \
        v_size + grid[:, 2] * v_size * v_size
    grid_d = grid_idx+dist
    idx_orig = torch.argsort(grid_d)

    # trick from https://github.com/rusty1s/pytorch_unique
    unique, inverse, counts = torch.unique_consecutive(
        grid_idx[idx_orig], return_inverse=True, return_counts=True)
    dense_voxels = counts >= 10
    perm = torch.arange(inverse.size(
        0), dtype=inverse.dtype, device=inverse.device)
    inverse, perm = inverse.flip([0]), perm.flip([0])

    """
    HACK: workaround to get the first item. scatter overwrites indices on gpu not sequentially
           -> you get random points in the voxel not the first one
    """ 
    p= perm.cpu()
    i=inverse.cpu()
    idx = torch.empty(unique.shape,dtype=p.dtype).scatter_(0, i, p)
    return idx_orig[idx].tolist()


# =========================
# ROS2 Msg helper functions
# =========================


def transform_pointcloud(
    cloud: pcl.PointCloud, transform: np.ndarray,
) -> pcl.PointCloud:

    cloud_array = np.ones((cloud.size, 4))
    cloud_array[:, :3] = cloud.to_array()
    out_cloud_array = transform @ cloud_array.T
    out_cloud_array = np.asarray(out_cloud_array[:3, :], dtype=np.float32).T
    out_cloud = pcl.PointCloud(out_cloud_array)
    return out_cloud


def pcdToROSMsg(cloud: pcl.PointCloud):
    msg = PointCloud2()
    if cloud.width == 0 and cloud.height == 0:
        print("Cloud must be dense!")
        raise RuntimeError
    else:
        if cloud.size != cloud.width * cloud.height:
            print("Width and Height do not match cloud size!")
            raise RuntimeError
        msg.height = cloud.height
        msg.width = cloud.width
    cloud_arr = cloud.to_array()
    msg.data = list(bytearray(cloud_arr))
    msg.point_step = len(bytearray(cloud_arr[0]))
    msg.row_step = msg.point_step * msg.width
    msg.is_dense = True
    return msg


def convert_to_pc2_msg(points, frame_id='odom'):

    msg = PointCloud2()
    msg.header = Header(frame_id=frame_id)

    points = points.to_array()

    if not points.size == 0:

        # fill in meta data
        msg.height = 1
        msg.width = len(points)
        msg.fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
        ]
        msg.is_bigendian = False
        msg.point_step = 12
        msg.row_step = 12 * points.shape[0]
        msg.is_dense = False

        # convert numpy points to bytes and pass to msg data
        buffer = []
        for point in points:
            buffer.append(struct.pack('fff', *point))
        msg.data = b''.join(buffer)
    
    return msg


def rosMsgToPcd(msg: PointCloud2):
    cloud = pcl.PointCloud()
    data = msg.data
    byte_arr = bytearray(data)
    cloud_arr = None
    coord_size = msg.point_step / 3
    f = ""
    for i in msg.fields:
        if i.datatype == 1:
            f += "c"
        elif i.datatype == 2:
            f += "B"
        elif i.datatype == 3:
            f += "i"
        elif i.datatype == 4:
            f += "I"
        elif i.datatype == 5:
            f += "l"
        elif i.datatype == 6:
            f += "L"
        elif i.datatype == 7:
            f += "f"
        elif i.datatype == 8:
            f += "d"
    cloud_arr = struct.unpack(f * msg.height * msg.width, byte_arr)
    cloud_arr = np.reshape(cloud_arr, (msg.height, msg.width, len(msg.fields)))
    cloud_arr = cloud_arr[:, :, :3]
    cloud_arr = cloud_arr.reshape(msg.height * msg.width, 3)
    cloud_arr = np.asarray(cloud_arr, dtype=np.float32)
    cloud.from_array(cloud_arr)
    return cloud


# =======================================
# Class definitions for sensor Extrinsics
# =======================================


class SE3:
    def __init__(self, R, t):
        self.R = R
        self.t = t


class Extrinsics:

    def __init__(self, baselink2imu: SE3, baselink2lidar: SE3):

        self.baselink2imu = baselink2imu
        self.baselink2lidar = baselink2lidar

        self.baselink2imu_T = np.eye(4)
        self.baselink2imu_T[:3, :3] = self.baselink2imu.R
        self.baselink2imu_T[:3, 3] = self.baselink2imu.t

        self.baselink2lidar_T = np.eye(4)
        self.baselink2lidar_T[:3, :3] = self.baselink2lidar.R
        self.baselink2lidar_T[:3, 3] = self.baselink2lidar.t


# =======================================
# Class definitions for sensor Intrinsics
# =======================================


class ImuBias:
    def __init__(self, gyro: np.array, accel: np.array):
        self.gyro = gyro
        self.accel = accel


class Frames:
    def __init__(self, b: np.array, w: np.array):
        self.b = b
        self.w = w


class Velocity:
    def __init__(self, lin: Frames, ang: Frames):
        self.lin = lin
        self.ang = ang


class State:
    def __init__(
        self,
        p: np.array,  # position
        q: np.array,  # quaternion
        v: Velocity,
        b: ImuBias,
    ):
        self.p = p
        self.q = q
        self.v = v
        self.b = b


# ==================================
# Class definitions for Point Clouds
# ==================================


class Keyframe:
    def __init__(self, translation, quaternion, pcd):
        self.translation = translation
        if not isinstance(quaternion, np.ndarray):
            quaternion = np.array(quaternion)
        self.quaternion = quaternion
        self.pcd = pcd


# =========================
# Class definitions for Imu
# =========================


class ImuMeas:
    def __init__(self, stamp, dt, ang_vel: np.array, lin_accel: np.array):
        self.stamp = stamp
        self.dt = dt
        self.ang_vel = ang_vel
        self.lin_accel = lin_accel


class ImuBuffer:

    def __init__(self, capacity):
        self.capacity = capacity
        self.buffer = []

    def push_front(self, imu_meas):
        self.buffer = [copy.deepcopy(imu_meas)] + self.buffer
        if len(self.buffer) > self.capacity:
            self.buffer = self.buffer[: self.capacity]

    def front(self):
        return self.buffer[0]

    def back(self):
        return self.buffer[-1]

    def empty(self):
        return len(self.buffer) == 0

    def get_all_measurement_in_time_range(self, start_time, end_time):

        end_imu_idx = self.find_last_after_end_time(end_time)
        begin_imu_idx = self.find_first_before_start_time(start_time)

        if begin_imu_idx == -1:
            return None

        return self.buffer[end_imu_idx : begin_imu_idx + 1]

    def find_last_after_end_time(self, end_time):

        idx = 0
        imu_ = self.buffer[0]

        while idx != len(self.buffer) and imu_.stamp >= end_time.nanoseconds/1e9:
            imu_ = self.buffer[idx]
            idx += 1

        return idx - 1

    def find_first_before_start_time(self, start_time):

        idx = 0
        imu_ = self.buffer[0]

        while idx != len(self.buffer) and imu_.stamp >= start_time.nanoseconds/1e9:
            idx += 1

        if idx == len(self.buffer):
            return -1

        return idx + 1


# ========================================
# Class definitions for Geometric Observer
# ========================================


class Geo:
    def __init__(
        self,
        first_opt_done: bool,
        dp: float,
        dp_deg: float,
        prev_p: np.array,
        prev_q: np.array,
        prev_vel: np.array,
    ):
        self.first_opt_done = first_opt_done
        self.mtx = threading.Lock()
        self.dp = dp
        self.dp_deg = dp_deg
        self.prev_p = prev_p
        self.prev_q = prev_q
        self.prev_vel = prev_vel


# ===========================
# Class definitions for Poses
# ===========================


class Pose_Struct:
    def __init__(self, p: np.array, q: np.array):
        self.p = p
        self.q = q


# ==================================
# Metric Class for debugging purpose
# ==================================


class Metric:
    def __init__(self):
        self.spaciousness = []
        self.density = []