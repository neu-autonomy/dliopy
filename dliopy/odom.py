import sys
import threading
import queue
import time
import os
import psutil
import copy

import torch
import numpy as np
from scipy.spatial import ConvexHull, Delaunay
from shapely.geometry import mapping
import pcl
import pygicp

# supporting classes and structs
from dliopy.utils import SE3, Extrinsics
from dliopy.utils import Keyframe
from dliopy.utils import ImuBias, Frames, Velocity, State
from dliopy.utils import ImuMeas, ImuBuffer
from dliopy.utils import Geo
from dliopy.utils import Pose_Struct
from dliopy.utils import Metric
from dliopy.utils import transform_pointcloud, pcdToROSMsg, rosMsgToPcd, convert_to_pc2_msg
from dliopy.utils import gridSampling
from dliopy.quaternion_utils import *

# ROS Related Import
import rclpy
import rclpy.duration
from rclpy.node import Node
from nav_msgs.msg import Odometry, Path
from geometry_msgs.msg import PoseStamped, PoseArray, Pose, TransformStamped
from sensor_msgs.msg import Imu, PointCloud2
from std_msgs.msg import Header
from tf2_ros import TransformBroadcaster

# PCL Related Import
"""
PCL itself is built for C++
To use it, I believe we can use python-pcl
It has all the imported pcl functions in original DLIO
Ref: https://python-pcl-fork.readthedocs.io/en/rc_patches4/install.html
I have not tried installing the library on Ubuntu 22.04 yet
Note: python3-pcl lib managed by Ubuntu official does not work because
    it does not have the convex hull and concave hull functionality
"""
import pcl


# Implementation for Odometry Node
class OdomNode(Node):

    def __init__(self):

        # Initialize Node with correct Namespace
        super().__init__("dliopy_odom_node", namespace="dliopy")

        # load parameters from yaml files
        self.getParams()

        # indicators
        self.dlio_initialized = False
        self.first_valid_scan = False
        self.first_imu_received = False
        if self.imu_calibrate_:
            self.imu_calibrated = False
        else:
            self.imu_calibrated = True
        self.deskew_status = False
        self.deskew_size = 0

        # Lidar Subscription
        self.lidar_sub = self.create_subscription(
            PointCloud2, "pointcloud", self.callbackPointCloud, 1
        )

        # IMU Subscription
        self.imu_sub = self.create_subscription(Imu, "imu", self.callbackImu, 1000)

        # Publishers
        self.odom_pub = self.create_publisher(Odometry, "odom", 1)
        self.pose_pub = self.create_publisher(PoseStamped, "pose", 1)
        self.path_pub = self.create_publisher(Path, "path", 1)
        self.kf_pose_pub = self.create_publisher(PoseArray, "kf_pose", 1)
        self.kf_cloud_pub = self.create_publisher(PointCloud2, "kf_cloud", 1)
        self.deskew_pub = self.create_publisher(PointCloud2, "deskewed", 1)

        # tf2 broadcaster
        self.br = TransformBroadcaster(self)

        # timed publisher
        self.publish_timer = self.create_timer(0.01, self.publishPose)

        # Threading Objects
        # mutex locks
        self.mtx_imu = threading.Lock()
        # condition variables
        self.cv_imu_stamp = threading.Condition()

        # parameters
        self.odom_ros = Odometry()
        self.pose_ros = PoseStamped()
        self.path_ros = Path()
        self.kf_pose_ros = PoseArray()

        self.mtx_keyframes = threading.Lock()
        self.keyframes = []
        self.keyframe_timestamps = []
        self.keyframe_normals = []
        self.keyframe_transformations = []

        self.T = np.eye(4)
        self.T_prior = np.eye(4)
        self.T_corr = np.eye(4)

        self.origin = np.zeros(3)

        self.lidarPose = Pose_Struct([0.0, 0.0, 0.0], [1.0, 0.0, 0.0, 0.0])

        # for imu calibration
        self.num_samples = 0
        self.gyro_avg = np.zeros(3)
        self.accel_avg = np.zeros(3)

        self.imu_meas = ImuMeas(0.0, 0.0, [0.0, 0.0, 0.0], [0.0, 0.0, 0.0])
        self.imu_buffer = ImuBuffer(self.imu_buffer_size_)
        self.imu_rates = []

        self.imu_stamp = self.get_clock().now()
        self.first_imu_stamp = 0.0
        self.prev_imu_stamp = 0.0

        self.scan_header_stamp = self.get_clock().now()
        self.scan_stamp = 0.0
        self.prev_scan_stamp = 0.0
        self.scan_dt = 0.0

        self.original_scan = pcl.PointCloud()
        self.deskewed_scan = pcl.PointCloud()
        self.current_scan = pcl.PointCloud()
        self.submap_cloud = pcl.PointCloud()

        self.num_processed_keyframes = 0
        self.map_size = 0

        self.submap_hasChanged = True
        self.submap_kf_idx_prev = []
        self.submap_kf_idx_curr = []

        self.first_scan_stamp = 0.0
        self.elapsed_time = 0.0
        self.length_traversed = 0.0

        # ================== TODO ==================
        # intializing GICP and PCL filters and hulls
        # ==========================================

        self.geo = Geo(
            False, 0.0, 0.0, np.zeros(3), np.array([1.0, 0.0, 0.0, 0.0]), np.zeros(3)
        )

        self.main_loop_running = False

        self.fast_gicp = pygicp.FastGICP()
        self.fast_gicp.set_max_correspondence_distance(self.gicp_max_corr_dist_)

        self.keyframe_convex = []
        self.keyframe_concave = []

        self.trajectory = []
        self.lidar_rates = []
        self.comp_times = []

        # Metric
        self.metric = Metric()
        self.metric.spaciousness.append(0.)
        self.metric.density.append(self.gicp_max_corr_dist_)

        # Static parameters in C++ implementation
        self.prev_stamp = None
        self.ang_vel_cg_prev = None
        self.print_indicator = True

    def getParams(self):

        # Version
        self.version = self.retrieve_param_from_yaml("version", "0.0.0").string_value

        # Frames
        self.odom_frame = self.retrieve_param_from_yaml(
            "frames/odom", "odom"
        ).string_value
        self.baselink_frame = self.retrieve_param_from_yaml(
            "frames/baselink", "baselink"
        ).string_value
        self.lidar_frame = self.retrieve_param_from_yaml(
            "frames/lidar", "lidar"
        ).string_value
        self.imu_frame = self.retrieve_param_from_yaml("frames/imu", "imu").string_value

        # Deskew Flag
        self.deskew_ = self.retrieve_param_from_yaml(
            "pointcloud/deskew", True
        ).bool_value

        # Gravity
        self.gravity_ = self.retrieve_param_from_yaml(
            "odom/gravity", 9.80665
        ).double_value

        # Keyframe Threshold
        self.keyframe_thresh_dist_ = self.retrieve_param_from_yaml(
            "odom/keyframe/threshD", 0.1
        ).double_value
        self.keyframe_thresh_rot_ = self.retrieve_param_from_yaml(
            "odom/keyframe/threshR", 1.0
        ).double_value

        # Submap
        self.submap_knn_ = self.retrieve_param_from_yaml(
            "odom/submap/keyframe/knn", 10
        ).integer_value
        self.submap_kcv_ = self.retrieve_param_from_yaml(
            "odom/submap/keyframe/kcv", 10
        ).integer_value
        self.submap_kcc_ = self.retrieve_param_from_yaml(
            "odom/submap/keyframe/kcc", 10
        ).integer_value

        # Dense map resolution
        # When testing should try modify to false
        self.densemap_filtered_ = self.retrieve_param_from_yaml(
            "map/dense/filtered", False
        ).bool_value

        # Wait until movement to publish map
        self.wait_until_move_ = self.retrieve_param_from_yaml(
            "map/waitUntilMove", True
        ).bool_value

        # Crop Box Filter
        self.crop_size_ = self.retrieve_param_from_yaml(
            "odom/preprocessing/cropBoxFilter/size", 1.0
        ).double_value

        # Voxel Grid Filter
        # When testing should try modify to false
        self.vf_use_ = self.retrieve_param_from_yaml(
            "pointcloud/voxelize", True
        ).bool_value
        self.vf_res_ = self.retrieve_param_from_yaml(
            "odom/preprocessing/voxelFilter/res", 0.05
        ).double_value

        # Adaptive Parameters
        self.adaptive_params_ = self.retrieve_param_from_yaml(
            "adaptive", True
        ).bool_value

        # Extrinsics
        t_default = [0.0, 0.0, 0.0]
        R_default = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        # Center of Gravity to IMU
        imu_t = self.retrieve_param_from_yaml(
            "extrinsics/baselink2imu/t", t_default
        ).double_array_value
        imu_t = np.array(imu_t).reshape((3))
        imu_R = self.retrieve_param_from_yaml(
            "extrinsics/baselink2imu/R", R_default
        ).double_array_value
        imu_R = np.array(imu_R).reshape((3, 3))
        baselink2imu = SE3(imu_R, imu_t)
        # Center of Gravity to LiDAR
        lidar_t = self.retrieve_param_from_yaml(
            "extrinsics/baselink2lidar/t", t_default
        ).double_array_value
        lidar_t = np.array(lidar_t).reshape((3))
        lidar_R = self.retrieve_param_from_yaml(
            "extrinsics/baselink2lidar/R", R_default
        ).double_array_value
        lidar_R = np.array(lidar_R).reshape((3, 3))
        baselink2lidar = SE3(lidar_R, lidar_t)
        # Create Extrinsics object
        self.extrinsics = Extrinsics(baselink2imu, baselink2lidar)

        # IMU
        self.calibrate_accel_ = self.retrieve_param_from_yaml(
            "odom/imu/calibration/accel", True
        ).bool_value
        self.calibrate_gyro_ = self.retrieve_param_from_yaml(
            "odom/imu/calibration/gyro", True
        ).bool_value
        self.imu_calib_time_ = self.retrieve_param_from_yaml(
            "odom/imu/calibration/time", 3.0
        ).double_value
        self.imu_buffer_size_ = self.retrieve_param_from_yaml(
            "odom/imu/bufferSize", 2000
        ).integer_value

        # IMU intrinsics
        self.gravity_align_ = self.retrieve_param_from_yaml(
            "odom/imu/approximateGravity", False
        ).bool_value
        self.imu_calibrate_ = self.retrieve_param_from_yaml(
            "imu/calibration", True
        ).bool_value
        prior_accel_bias = self.retrieve_param_from_yaml(
            "imu/intrinsics/accel/bias", [0.0, 0.0, 0.0]
        ).double_array_value
        prior_gyro_bias = self.retrieve_param_from_yaml(
            "imu/intrinsics/gyro/bias", [0.0, 0.0, 0.0]
        ).double_array_value
        imu_sm = self.retrieve_param_from_yaml(
            "imu/intrinsics/accel/sm", [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        ).double_array_value

        b = None
        if not self.imu_calibrate_:
            b = ImuBias(prior_gyro_bias, prior_accel_bias)
            self.imu_accel_sm_ = np.array(imu_sm).reshape((3, 3))
        else:
            b = ImuBias([0.0, 0.0, 0.0], [0.0, 0.0, 0.0])
            self.imu_accel_sm_ = np.eye(3)

        p = np.zeros(3)
        q = np.array([1.0, 0.0, 0.0, 0.0])
        lin_b = np.zeros(3)
        lin_w = np.zeros(3)
        lin = Frames(lin_b, lin_w)
        ang_b = np.zeros(3)
        ang_w = np.zeros(3)
        ang = Frames(ang_b, ang_w)
        v = Velocity(lin, ang)
        self.state = State(p, q, v, b)

        # GICP
        self.gicp_min_num_points_ = self.retrieve_param_from_yaml(
            "odom/gicp/minNumPoints", 100
        ).integer_value
        self.gicp_k_correspondences_ = self.retrieve_param_from_yaml(
            "odom/gicp/kCorrespondences", 20
        ).integer_value
        self.gicp_max_corr_dist_ = self.retrieve_param_from_yaml(
            "odom/gicp/maxCorrespondenceDistance", np.sqrt(sys.float_info.max)
        ).double_value
        self.gicp_max_iter_ = self.retrieve_param_from_yaml(
            "odom/gicp/maxIterations", 64
        ).integer_value
        self.gicp_transformation_eq_ = self.retrieve_param_from_yaml(
            "odom/gicp/transformationEpsilon", 0.0005
        ).double_value
        self.gicp_rotation_eq_ = self.retrieve_param_from_yaml(
            "odom/gicp/rotationEpsilon", 0.0005
        ).double_value
        self.gicp_init_lambda_factor_ = self.retrieve_param_from_yaml(
            "odom/gicp/initLambdaFactor", 1e-9
        ).double_value

        # Geometric Observer
        self.geo_Kp_ = self.retrieve_param_from_yaml("odom/geo/Kp", 1.0).double_value
        self.geo_Kv_ = self.retrieve_param_from_yaml("odom/geo/Kv", 1.0).double_value
        self.geo_Kq_ = self.retrieve_param_from_yaml("odom/geo/Kq", 1.0).double_value
        self.geo_Kab_ = self.retrieve_param_from_yaml("odom/geo/Kab", 1.0).double_value
        self.geo_Kgb_ = self.retrieve_param_from_yaml("odom/geo/Kgb", 1.0).double_value
        self.geo_abias_max_ = self.retrieve_param_from_yaml(
            "odom/geo/abias_max", 1.0
        ).double_value
        self.geo_gbias_max_ = self.retrieve_param_from_yaml(
            "odom/geo/gbias_max", 1.0
        ).double_value

    def retrieve_param_from_yaml(self, param_name, default_value):

        self.declare_parameter(param_name, default_value)
        return self.get_parameter(param_name).get_parameter_value()

    def publishPose(self):

        self.odom_ros.header.stamp = self.imu_stamp.to_msg()
        self.odom_ros.header.frame_id = self.odom_frame
        self.odom_ros.child_frame_id = self.baselink_frame

        self.odom_ros.pose.pose.position.x = self.state.p[0]
        self.odom_ros.pose.pose.position.y = self.state.p[1]
        self.odom_ros.pose.pose.position.z = self.state.p[2]

        self.odom_ros.pose.pose.orientation.w = self.state.q[0]
        self.odom_ros.pose.pose.orientation.x = self.state.q[1]
        self.odom_ros.pose.pose.orientation.y = self.state.q[2]
        self.odom_ros.pose.pose.orientation.z = self.state.q[3]

        self.odom_ros.twist.twist.linear.x = self.state.v.lin.w[0]
        self.odom_ros.twist.twist.linear.y = self.state.v.lin.w[1]
        self.odom_ros.twist.twist.linear.z = self.state.v.lin.w[2]

        self.odom_ros.twist.twist.angular.x = self.state.v.ang.b[0]
        self.odom_ros.twist.twist.angular.y = self.state.v.ang.b[1]
        self.odom_ros.twist.twist.angular.z = self.state.v.ang.b[2]

        self.odom_pub.publish(self.odom_ros)

        self.pose_ros.header.stamp = self.imu_stamp.to_msg()
        self.pose_ros.header.frame_id = self.odom_frame

        self.pose_ros.pose.position.x = self.state.p[0]
        self.pose_ros.pose.position.y = self.state.p[1]
        self.pose_ros.pose.position.z = self.state.p[2]

        self.pose_ros.pose.orientation.w = self.state.q[0]
        self.pose_ros.pose.orientation.x = self.state.q[1]
        self.pose_ros.pose.orientation.y = self.state.q[2]
        self.pose_ros.pose.orientation.z = self.state.q[3]

        self.pose_pub.publish(self.pose_ros)

    def publishToROS(self, published_cloud, T_cloud):

        self.publishCloud(published_cloud, T_cloud)

        # nav_msgs.msg.PATH
        self.path_ros.header.stamp = self.imu_stamp.to_msg()
        self.path_ros.header.frame_id = self.odom_frame

        p = PoseStamped()
        p.header.stamp = self.imu_stamp.to_msg()
        p.header.frame_id = self.odom_frame
        p.pose.position.x = self.state.p[0]
        p.pose.position.y = self.state.p[1]
        p.pose.position.z = self.state.p[2]
        p.pose.orientation.w = self.state.q[0]
        p.pose.orientation.x = self.state.q[1]
        p.pose.orientation.y = self.state.q[2]
        p.pose.orientation.z = self.state.q[3]

        self.path_ros.poses.append(p)
        self.path_pub.publish(self.path_ros)
        self.pose_pub.publish(p)

        transformStamped = TransformStamped()

        # transform: odom to baselink
        transformStamped.header.stamp = self.imu_stamp.to_msg()
        transformStamped.header.frame_id = self.odom_frame
        transformStamped.child_frame_id = self.baselink_frame

        transformStamped.transform.translation.x = self.state.p[0]
        transformStamped.transform.translation.y = self.state.p[1]
        transformStamped.transform.translation.z = self.state.p[2]

        transformStamped.transform.rotation.w = self.state.q[0]
        transformStamped.transform.rotation.x = self.state.q[1]
        transformStamped.transform.rotation.y = self.state.q[2]
        transformStamped.transform.rotation.z = self.state.q[3]

        self.br.sendTransform(transformStamped)

        # transform: baselink to imu
        transformStamped.header.stamp = self.imu_stamp.to_msg()
        transformStamped.header.frame_id = self.baselink_frame
        transformStamped.child_frame_id = self.imu_frame

        transformStamped.transform.translation.x = self.extrinsics.baselink2imu.t[0]
        transformStamped.transform.translation.y = self.extrinsics.baselink2imu.t[1]
        transformStamped.transform.translation.z = self.extrinsics.baselink2imu.t[2]

        q = rotation_matrix_to_quaternion(self.extrinsics.baselink2imu.R)
        transformStamped.transform.rotation.w = q[0]
        transformStamped.transform.rotation.x = q[1]
        transformStamped.transform.rotation.y = q[2]
        transformStamped.transform.rotation.z = q[3]

        self.br.sendTransform(transformStamped)

        # transform: baselink to lidar
        transformStamped.header.stamp = self.imu_stamp.to_msg()
        transformStamped.header.frame_id = self.baselink_frame
        transformStamped.child_frame_id = self.lidar_frame

        transformStamped.transform.translation.x = self.extrinsics.baselink2lidar.t[0]
        transformStamped.transform.translation.y = self.extrinsics.baselink2lidar.t[1]
        transformStamped.transform.translation.z = self.extrinsics.baselink2lidar.t[2]

        qq = rotation_matrix_to_quaternion(self.extrinsics.baselink2lidar.R)
        transformStamped.transform.rotation.w = qq[0]
        transformStamped.transform.rotation.x = qq[1]
        transformStamped.transform.rotation.y = qq[2]
        transformStamped.transform.rotation.z = qq[3]

        self.br.sendTransform(transformStamped)

    def publishCloud(self, published_cloud, T_cloud):

        if self.wait_until_move_:
            if self.length_traversed < 0.1:
                return
        
        deskewed_scan_t_ = transform_pointcloud(
            published_cloud, 
            T_cloud
        )

        publish_cloud_ros = convert_to_pc2_msg(deskewed_scan_t_)
        publish_cloud_ros.header = Header(frame_id = self.odom_frame)
        self.deskew_pub.publish(publish_cloud_ros)

    def publishKeyframe(self, keyframe, timestamp):
        # Push back
        p = Pose()
        p.position.x = keyframe.translation[0]
        p.position.y = keyframe.translation[1]
        p.position.z = keyframe.translation[2]
        p.orientation.w = keyframe.quaternion[0]
        p.orientation.x = keyframe.quaternion[1]
        p.orientation.y = keyframe.quaternion[2]
        p.orientation.z = keyframe.quaternion[3]
        self.kf_pose_ros.poses.append(p)

        # Publish
        self.kf_pose_ros.header.stamp = timestamp.to_msg()
        self.kf_pose_ros.header.frame_id = self.odom_frame
        self.kf_pose_pub.publish(self.kf_pose_ros)

        # Publish keframe scan for map
        if self.vf_use_:
            # if keyframe.pcd.size == keyframe.pcd.width * keyframe.pcd.height:
            keyframe_cloud_ros = convert_to_pc2_msg(keyframe.pcd)
            keyframe_cloud_ros.header = Header(frame_id = self.odom_frame)
            self.kf_cloud_pub.publish(keyframe_cloud_ros)
        else:
            keyframe_cloud_ros = convert_to_pc2_msg(keyframe.pcd)
            keyframe_cloud_ros.header = Header(frame_id = self.odom_frame)
            self.kf_cloud_pub.publish(keyframe_cloud_ros)

    def getScanFromROS(self, pc: PointCloud2):
        self.original_scan = rosMsgToPcd(pc)
        self.scan_header_stamp = rclpy.time.Time().from_msg(pc.header.stamp)

    def preprocessPoints(self):

        # if self.deskew_:

        #     # ====== TODO ======
        #     # Implement Deskewing
        #     pass

        # else:

        self.scan_stamp = self.scan_header_stamp

        # don't process scans untill IMU data is present

        if not self.first_valid_scan:
 
            if self.imu_buffer.empty() or self.scan_stamp.nanoseconds / 1e9 <= self.imu_buffer.back().stamp:
                return
            
            self.first_valid_scan = True
            self.T_prior = self.T
        
        else:

            # frames = self.integrateImu(
            #     self.prev_scan_stamp, 
            #     self.lidarPose.q, 
            #     self.lidarPose.p, 
            #     self.geo.prev_vel, 
            #     [self.scan_stamp]
            # )

            # if frames.shape[0] > 0:
            #     self.T_prior = frames[-1]
            # else:
            self.T_prior = self.T
        
        self.deskewed_scan = transform_pointcloud(
            self.original_scan, 
            self.T_prior @ self.extrinsics.baselink2lidar_T
        )
        self.deskew_status = False

        scan = torch.tensor(self.deskewed_scan.to_array())
        # remove all points within 1 meter from origin
        indices = torch.norm(scan, dim=1) > 1.0
        scan = scan[indices]
        # Voxel Grid Sampling
        indices = gridSampling(scan, resolution=self.vf_res_)
        scan = scan[indices].numpy()
        self.current_scan = pcl.PointCloud(scan)


    def deskewPointCloud(self):

        raise NotImplementedError

    def initializeInputTarget(self):

        self.prev_scan_stamp = self.scan_stamp

        # Keep history of keyframes
        self.keyframes.append(
            Keyframe(self.lidarPose.p, self.lidarPose.q, self.current_scan)
        )
        self.keyframe_timestamps.append(self.scan_header_stamp)
        self.keyframe_normals.append(self.fast_gicp.get_source_covariances())
        self.keyframe_transformations.append(self.T_corr)

    def setInputSource(self):

        # self.gicp.setInputSource(self.current_scan)
        # self.gicp.calculateSourceCovariances()
        self.fast_gicp.set_input_source(self.current_scan)

    def initializeDLIO(self):

        # Wait for IMU
        if not self.first_imu_received or not self.imu_calibrated:
            return

        self.dlio_initialized = True
        print("DLIO initialized!")

    def callbackPointCloud(self, msg):
        self.main_loop_running = True

        then = time.time()
        if self.first_scan_stamp == 0.0:
            self.first_scan_stamp = (
                rclpy.time.Time().from_msg(msg.header.stamp).nanoseconds
            )

        if not self.dlio_initialized:
            self.initializeDLIO()

        self.getScanFromROS(msg)
        self.preprocessPoints()

        if self.current_scan.size <= self.gicp_min_num_points_:
            self.get_logger().fatal("Low number of points in cloud!")
            return

        self.computeMetrics()

        if self.adaptive_params_:
            self.setAdaptiveParams()

        self.setInputSource()

        if len(self.keyframes) == 0:
            self.initializeInputTarget()
            self.main_loop_running = False
            self.buildKeyframesAndSubmap(self.state)
            print("First scan returned")
            return

        self.getNextPose()
        self.updateKeyframes()

        self.buildKeyframesAndSubmap(self.state)

        self.trajectory.append(copy.deepcopy(self.state))

        if isinstance(self.scan_stamp, rclpy.time.Time) or \
            isinstance(self.prev_scan_stamp, rclpy.time.Time):
            self.lidar_rates.append(1. / ((self.scan_stamp - self.prev_scan_stamp).nanoseconds / 1e9) )
        else:
            self.lidar_rates.append(1. / (self.scan_stamp - self.prev_scan_stamp))
        self.prev_scan_stamp = self.scan_stamp
        self.elapsed_time = self.scan_stamp.nanoseconds/1e9 - self.first_scan_stamp/1e9

        published_cloud = pcl.PointCloud()
        if self.densemap_filtered_:
            published_cloud = self.current_scan
        else:
            published_cloud = self.deskewed_scan
        self.publishToROS(published_cloud, self.T_corr)

        self.comp_times.append(time.time() - then)

        # These aren't used? They also aren't implemented yet in out fast_gicp fork
        # self.gicp_hasConverged = self.fast_gicp.hasConverged()

        self.debug()
        self.geo.first_opt_done = True

    def callbackImu(self, imu_raw):

        self.first_imu_received = True

        imu = self.transformImu(imu_raw)
        self.imu_stamp = rclpy.time.Time().from_msg(imu.header.stamp)
        imu_stamp_secs = rclpy.time.Time().from_msg(imu.header.stamp).nanoseconds / 1e9

        # get IMU samples
        ang_vel = np.array(
            [imu.angular_velocity.x, imu.angular_velocity.y, imu.angular_velocity.z]
        )

        lin_accel = np.array(
            [
                imu.linear_acceleration.x,
                imu.linear_acceleration.y,
                imu.linear_acceleration.z,
            ]
        )

        if self.first_imu_stamp == 0.0:
            self.first_imu_stamp = imu_stamp_secs

        # imu calibration procedure, do for 3 sec
        if not self.imu_calibrated:

            if imu_stamp_secs - self.first_imu_stamp < self.imu_calib_time_:

                self.num_samples += 1

                self.gyro_avg += ang_vel
                self.accel_avg += lin_accel

                if self.print_indicator:

                    print(f"Calibrating IMU for {self.imu_calib_time_} seconds...")
                    sys.stdout.flush()
                    self.print_indicator = False

            else:

                print("Imu Calibration Done")

                self.gyro_avg /= self.num_samples
                self.accel_avg /= self.num_samples

                grav_vel = np.array([0.0, 0.0, self.gravity_])

                if self.gravity_align_:

                    # estimate gravity vector - Only approximate if biases have not been pre-calibrated
                    # ignoring this part in the yaml config gravity_align_ flag is set to False
                    pass

                if self.calibrate_accel_:

                    # subtract gravity from avg accel to get bias
                    self.state.b.accel = self.accel_avg - grav_vel
                    print(f"Accel Biases [xyz]: {self.state.b.accel}")

                if self.calibrate_gyro_:

                    self.state.b.gyro = self.gyro_avg
                    print(f"Gyro Biases [xyz]: {self.state.b.gyro}")

                self.imu_calibrated = True

        else:

            dt = imu_stamp_secs - self.prev_imu_stamp

            if dt == 0:
                dt = 1.0 / 200.0

            self.imu_rates.append(1.0 / dt)

            self.imu_meas.stamp = imu_stamp_secs
            self.imu_meas.dt = dt
            self.prev_imu_stamp = self.imu_meas.stamp

            self.imu_meas.lin_accel = (
                np.matmul(self.imu_accel_sm_, lin_accel) - self.state.b.accel
            )
            self.imu_meas.ang_vel = ang_vel - self.state.b.gyro

            # store calibrated IMU measurements into imu buffer for manual integration later
            with self.mtx_imu:
                self.imu_buffer.push_front(self.imu_meas)

            # Notify the callbackPointCloud threshold that IMU data exists for this time
            # ============================== TODO ==============================
            # Currently we donnot have a acquiring block of conditional variable
            # So the line below is commented out
            # Remember to uncomment when we implement point cloud callback
            with self.cv_imu_stamp:
                self.cv_imu_stamp.notify()

            if self.geo.first_opt_done:
                # Geometric observer: propagate state
                self.propagateState()

    def getNextPose(self):
        # Not async, dont check for new submap

        if self.submap_hasChanged:
            self.fast_gicp.set_input_target(self.submap_cloud)
            self.fast_gicp.set_target_covariances(self.submap_normals)
            self.submap_hasChanged = False

        self.T_corr = self.fast_gicp.align()
        self.T = np.dot(self.T_corr, self.T_prior)

        self.propagateGICP()
        self.updateState()

    def imuMeasFromTimeRange(self, start_time, end_time):

        if self.imu_buffer.empty() or self.imu_buffer.front().stamp < end_time.nanoseconds/1e9:

            with self.cv_imu_stamp:
                self.cv_imu_stamp.wait_for(lambda: self.imu_buffer.front().stamp >= end_time.nanoseconds/1e9)

        with self.mtx_imu:

            imu_measurements = self.imu_buffer.get_all_measurement_in_time_range(
                start_time, end_time
            )

            if imu_measurements == None:
                return False, None

            return True, imu_measurements

    def integrateImu(self, start_time, q_init, p_init, v_init, sorted_timestamps):

        empty = np.empty(0)

        if len(sorted_timestamps) == 0 or start_time > sorted_timestamps[0]:
            return empty

        indicator, imu_measurements = self.imuMeasFromTimeRange(
            start_time, sorted_timestamps[-1]
        )
        if not indicator:
            return empty

        end_imu = imu_measurements[0]
        begin_imu = imu_measurements[-1]
        one_after_begin_imu = imu_measurements[-2]

        # Time between first two IMU samples
        dt = one_after_begin_imu.dt

        # Time between first IMU sample and start_time
        idt = start_time - begin_imu.dt

        # Angular acceleration between first two IMU samples
        alpha_dt = one_after_begin_imu.ang_vel - begin_imu.ang_vel
        alpha = alpha_dt / dt

        # Average angular velocity (reversed) between first IMU sample and start_time
        omega_i = -(begin_imu.ang_vel + 0.5 * alpha * idt)

        # Set q_init to orientation at first IMU sample
        q_init = np.array(
            [
                q_init[0]
                - 0.5
                * (
                    q_init[1] * omega_i[0]
                    + q_init[2] * omega_i[1]
                    + q_init[3] * omega_i[2]
                )
                * idt,
                q_init[1]
                + 0.5
                * (
                    q_init[0] * omega_i[0]
                    - q_init[3] * omega_i[1]
                    + q_init[2] * omega_i[2]
                )
                * idt,
                q_init[2]
                + 0.5
                * (
                    q_init[3] * omega_i[0]
                    + q_init[0] * omega_i[1]
                    - q_init[1] * omega_i[2]
                )
                * idt,
                q_init[3]
                + 0.5
                * (
                    q_init[1] * omega_i[1]
                    - q_init[2] * omega_i[0]
                    + q_init[0] * omega_i[2]
                )
                * idt,
            ]
        )
        q_init = q_init / np.linalg.norm(q_init)

        # Average angular velocity between first two IMU samples
        omega = begin_imu.ang_vel + 0.5 * alpha_dt

        # Orientation at second IMU sample
        q2 = np.array(
            [
                q_init[0]
                - 0.5
                * (q_init[1] * omega[0] + q_init[2] * omega[1] + q_init[3] * omega[2])
                * dt,
                q_init[1]
                + 0.5
                * (q_init[0] * omega[0] - q_init[3] * omega[1] + q_init[2] * omega[2])
                * dt,
                q_init[2]
                + 0.5
                * (q_init[3] * omega[0] + q_init[0] * omega[1] - q_init[1] * omega[2])
                * dt,
                q_init[3]
                + 0.5
                * (q_init[1] * omega[1] - q_init[2] * omega[0] + q_init[0] * omega[2])
                * dt,
            ]
        )
        q2 = q2 / np.linalg.norm(q2)

        # Acceleration at first IMU sample
        a1 = rotate_vector_by_quaternion(begin_imu.lin_accel, q_init)
        a1[2] -= self.gravity_

        # Accelration at second IMU sample
        a2 = rotate_vector_by_quaternion(one_after_begin_imu.lin_accel, q2)
        a2[2] -= self.gravity_

        # Jerk between first two IMU samples
        j = (a2 - a1) / dt

        # Set v_init to velocity at first IMU sample (go backwards from start_time)
        v_init -= a1 * idt + 0.5 * j * idt**2

        # Set p_init to position at first IMU sample (go backwards from start_time)
        p_init -= v_init * idt + 0.5 * a1 * idt**2 + (1 / 6.0) * j * idt**3

        return self.integrateImuInternal(
            q_init, p_init, v_init, sorted_timestamps, imu_measurements
        )

    def integrateImuInternal(
        self, q_init, p_init, v_init, sorted_timestamps, imu_measurements
    ):

        imu_se3 = []

        end_imu = imu_measurements[0]
        begin_imu = imu_measurements[-1]

        q = q_init
        p = p_init
        v = v_init
        a = rotate_vector_by_quaternion(begin_imu.lin_accel, q)
        a[2] -= self.gravity_

        # Iterate over IMU measurements and timestamps
        prev_imu_idx = 0
        imu_idx = 1

        # time stamp idx
        stamp_index = 0

        while imu_idx != len(imu_measurements):

            f0 = imu_measurements[prev_imu_idx]
            f = imu_measurements[imu_idx]

            # Time between IMU samples
            dt = f.dt

            # Angular acceleration
            alpha_dt = f.ang_vel - f0.ang_vel
            alpha = alpha_dt / dt

            # Average angular velocity
            omega = f0.ang_vel + 0.5 * alpha_dt

            # Orientation
            q = np.array(
                [
                    q[0]
                    - 0.5 * (q[1] * omega[0] + q[2] * omega[1] + q[3] * omega[2]) * dt,
                    q[1]
                    + 0.5 * (q[0] * omega[0] - q[3] * omega[1] + q[2] * omega[2]) * dt,
                    q[2]
                    + 0.5 * (q[3] * omega[0] + q[0] * omega[1] - q[1] * omega[2]) * dt,
                    q[3]
                    + 0.5 * (q[1] * omega[1] - q[2] * omega[0] + q[0] * omega[2]) * dt,
                ]
            )
            q = q / np.linalg.norm(q)

            # Acceleration
            a0 = a
            a = rotate_vector_by_quaternion(f.lin_accel, q)
            a[2] -= self.gravity_

            # Jerk
            j_dt = a - a0
            j = j_dt / dt

            # interpolate for given timestamps
            while (
                stamp_idx != len(sorted_timestamps)
                and sorted_timestamps[stamp_idx] <= f.stamp
            ):

                # Time between previous IMU sample and given timestamp
                idt = sorted_timestamps[stamp_idx] - f0.stamp

                # Average angular velocity
                omega_i = f0.ang_vel + 0.5 * alpha * idt

                # Orientation
                q_i = np.array(
                    [
                        q[0]
                        - 0.5
                        * (q[1] * omega_i[0] + q[2] * omega_i[1] + q[3] * omega_i[2])
                        * idt,
                        q[1]
                        + 0.5
                        * (q[0] * omega_i[0] - q[3] * omega_i[1] + q[2] * omega_i[2])
                        * idt,
                        q[2]
                        + 0.5
                        * (q[3] * omega_i[0] + q[0] * omega_i[1] - q[1] * omega_i[2])
                        * idt,
                        q[3]
                        + 0.5
                        * (q[1] * omega_i[1] - q[2] * omega_i[0] + q[0] * omega_i[2])
                        * idt,
                    ]
                )
                q_i = q_i / np.linalg.norm(q_i)

                # Position
                p_i = p + v * idt + 0.5 * a0 * idt**2 + (1 / 6.0) * j * idt**3

                # Transformation
                T = np.eye(4)
                T[:3, :3] = quaternion_to_rotation_matrix(q_i)
                T[:3, 3] = p_i

                imu_se3.append(T)

                stamp_idx += 1

            # Position
            p += v * dt + 0.5 * a0 * dt**2 + (1 / 6.0) * j_dt * dt**2

            # Velocity
            v += a0 * dt + 0.5 * j_dt * dt

            prev_imu_idx = imu_idx
            imu_idx += 1

        imu_se3 = np.stack(imu_se3)

        return imu_se3

    def propagateGICP(self):

        self.lidarPose.p = self.T[:3, 3]

        rotSO3 = self.T[:3, :3]
        q = rotation_matrix_to_quaternion(rotSO3)

        # normalize quaternion
        self.lidarPose.q = q / np.linalg.norm(q)

    def propagateState(self):

        # lock thread to prevent state from being accessed by updateState
        with self.geo.mtx:

            dt = self.imu_meas.dt

            qhat = self.state.q

            # Transform accel from body to world frame
            world_accel = rotate_vector_by_quaternion(self.imu_meas.lin_accel, qhat)
            # Account for gravity
            world_accel[2] -= self.gravity_

            # Accel propagation
            self.state.p += dt * self.state.v.lin.w + 0.5 * dt**2 * world_accel
            self.state.v.lin.w += dt * world_accel
            self.state.v.lin.b = np.matmul(
                np.linalg.inv(quaternion_to_rotation_matrix(self.state.q)),
                self.state.v.lin.w,
            )

            # Gyro propagation
            omega = np.array(
                [
                    0.0,
                    self.imu_meas.ang_vel[0],
                    self.imu_meas.ang_vel[1],
                    self.imu_meas.ang_vel[2],
                ]
            )
            tmp = quaternion_multiplication(qhat, omega)
            self.state.q += 0.5 * dt * tmp

            # Ensure quaternion is properly normalized
            self.state.q = self.state.q / np.linalg.norm(self.state.q)

            self.state.v.ang.b = self.imu_meas.ang_vel
            self.state.v.ang.w = np.matmul(
                quaternion_to_rotation_matrix(self.state.q), self.state.v.ang.b
            )

    def updateState(self):

        with self.geo.mtx:

            pin = self.lidarPose.p
            qin = self.lidarPose.q
            dt = self.scan_stamp - self.prev_scan_stamp

            if isinstance(dt, rclpy.duration.Duration):
                dt = dt.nanoseconds / 1e9

            qhat = self.state.q

            # construct error quaternion
            qe = quaternion_multiplication(quaternion_conjugate(qhat), qin)

            sgn = 1.0
            if qe[0] < 0:
                sgn = -1.0

            # construct quaterion correction
            qcorr = np.zeros(4)
            qcorr[0] = 1 - np.abs(qe[0])
            qcorr[1:] = sgn * qe[1:]
            qcorr = quaternion_multiplication(qhat, qcorr)

            err = pin - self.state.p
            err_body = rotate_vector_by_quaternion(err, quaternion_conjugate(qhat))

            abias_max = self.geo_abias_max_
            gbias_max = self.geo_gbias_max_

            # Update accel bias
            self.state.b.accel -= dt * self.geo_Kab_ * err_body
            self.state.b.accel = np.clip(self.state.b.accel, -abias_max, abias_max)

            # Update gyro bias
            self.state.b.gyro[0] -= dt * self.geo_Kgb_ * qe[0] * qe[1]
            self.state.b.gyro[1] -= dt * self.geo_Kgb_ * qe[0] * qe[2]
            self.state.b.gyro[1] -= dt * self.geo_Kgb_ * qe[0] * qe[3]
            self.state.b.gyro = np.clip(self.state.b.gyro, -gbias_max, gbias_max)

            # Update state
            self.state.p += dt * self.geo_Kp_ * err
            self.state.v.lin.w += dt * self.geo_Kv_ * err

            self.state.q = self.state.q + dt * self.geo_Kq_ * qcorr
            self.state.q = self.state.q / np.linalg.norm(self.state.q)

            # Store previous pose, orientation, and velocity
            self.geo.prev_p = self.state.p
            self.geo.prev_q = self.state.q
            self.geo.prev_vel = self.state.v.lin.w

    def transformImu(self, imu_raw):

        imu = Imu()

        # copy header
        imu.header = imu_raw.header

        imu_stamp_secs = rclpy.time.Time().from_msg(imu.header.stamp).nanoseconds / 1e9
        self.prev_stamp = imu_stamp_secs
        dt = imu_stamp_secs - self.prev_stamp
        self.prev_stamp = imu_stamp_secs

        if dt == 0:
            dt = 1.0 / 200.0

        # transform angular velocity (will be the same on a rigid body, just rotate to ROS convention)
        ang_vel = np.array(
            [
                imu_raw.angular_velocity.x,
                imu_raw.angular_velocity.y,
                imu_raw.angular_velocity.z,
            ]
        )

        ang_vel_cg = np.matmul(self.extrinsics.baselink2imu.R, ang_vel)

        imu.angular_velocity.x = ang_vel_cg[0]
        imu.angular_velocity.y = ang_vel_cg[1]
        imu.angular_velocity.z = ang_vel_cg[2]

        self.ang_vel_cg_prev = ang_vel_cg

        # transform linear acceleration (need to account for component due to translational difference)
        lin_accel = np.array(
            [
                imu_raw.linear_acceleration.x,
                imu_raw.linear_acceleration.y,
                imu_raw.linear_acceleration.z,
            ]
        )

        lin_accel_cg = np.matmul(self.extrinsics.baselink2imu.R, lin_accel)

        lin_accel_cg = (
            lin_accel_cg
            + np.cross(
                (ang_vel_cg - self.ang_vel_cg_prev) / dt, -self.extrinsics.baselink2imu.t
            )
            + np.cross(
                ang_vel_cg, np.cross(ang_vel_cg, -self.extrinsics.baselink2imu.t)
            )
        )

        self.ang_vel_cg_prev = ang_vel_cg

        imu.linear_acceleration.x = lin_accel_cg[0]
        imu.linear_acceleration.y = lin_accel_cg[1]
        imu.linear_acceleration.z = lin_accel_cg[2]

        return imu

    def computeMetrics(self):

        self.computeSpaciousness()
        # self.computeDensity()

    def computeSpaciousness(self):

        # conver pcl point cloud to numpy
        scan = np.asarray(self.original_scan)
        # compute median range of points
        scan_xy = scan[:, :2]
        spaciousness = np.median(np.sqrt(np.sum(scan_xy ** 2, axis=-1)))

        self.metric.spaciousness.append(
            self.metric.spaciousness[-1] * 0.95 + spaciousness * 0.05
        )

    def computeDensity(self):

        if not self.geo.first_opt_done:
            density = 0
        else:
            density = self.fast_gicp.source_density_
        
        self.metric.density.append(
            self.metric.density[-1] * 0.95 + density * 0.05
        )

    def computeConvexHull(self):

        if self.num_processed_keyframes < 4:
            return
        
        cloud = []
        with self.mtx_keyframes:
            for i in range(self.num_processed_keyframes):
                cloud.append(self.keyframes[i].translation)
            cloud = np.stack(cloud)
        
        hull = ConvexHull(cloud)
        self.keyframe_convex = hull.vertices

    def computeConcaveHull(self):

        if self.num_processed_keyframes < 5:
            return
        
        cloud = []
        with self.mtx_keyframes:
            for i in range(self.num_processed_keyframes):
                cloud.append(self.keyframes[i].translation)
            cloud = np.stack(cloud)
        
        delaunay = Delaunay(cloud)
        hull_simplices = [
            simplex for simplex in delaunay.simplices \
                if np.any(np.linalg.norm(cloud[simplex] - np.mean(cloud[simplex], axis=0), axis=1) < self.keyframe_thresh_dist_)
            ]
        indices = np.unique(np.hstack(hull_simplices))
        self.keyframe_concave = indices


    def updateKeyframes(self):
        closest_d = np.finfo(np.float32).max
        closest_idx = 0
        keyframes_idx = 0

        num_nearby = 0

        for k in self.keyframes:
            delta_d = np.sqrt(
                (self.state.p[0] - k.translation[0]) ** 2
                + (self.state.p[1] - k.translation[1]) ** 2
                + (self.state.p[2] - k.translation[2]) ** 2
            )

            if delta_d < self.keyframe_thresh_dist_ * 1.5:
                num_nearby += 1

            if delta_d < closest_d:
                closest_d = delta_d
                closest_idx = keyframes_idx

            keyframes_idx += 1

        closest_pose = self.keyframes[closest_idx].translation
        closest_pose_r = self.keyframes[closest_idx].quaternion

        dd = np.sqrt(
            (self.state.p[0] - closest_pose[0]) ** 2
            + (self.state.p[1] - closest_pose[1]) ** 2
            + (self.state.p[2] - closest_pose[2]) ** 2
        )

        dq = None
        if np.dot(self.state.q, closest_pose_r) < 0.0:
            lq = closest_pose_r
            lq = -1 * lq
            dq = quaternion_multiplication(
                self.state.q,
                quaternion_inverse(lq)
            )
        else:
            dq = quaternion_multiplication(
                self.state.q,
                quaternion_inverse(closest_pose_r)
            )

        theta_rad = 2.0 * np.arctan2(
            np.sqrt((dq[1] ** 2) + (dq[2] ** 2) + dq[3] ** 2), dq[0]
        )
        theta_deg = theta_rad * (180.0 / np.pi)

        new_keyframe = False

        if (
            np.absolute(dd) > self.keyframe_thresh_dist_
            or np.absolute(theta_deg) > self.keyframe_thresh_rot_
        ):
            new_keyframe = True

        if np.absolute(dd) <= self.keyframe_thresh_dist_:
            new_keyframe = False

        if (
            np.absolute(dd) <= self.keyframe_thresh_dist_
            and np.absolute(theta_deg) > self.keyframe_thresh_rot_
            and num_nearby <= 1
        ):
            new_keyframe = True

        if new_keyframe:
            self.keyframes.append(
                Keyframe(self.lidarPose.p, self.lidarPose.q, self.current_scan)
            )
            self.keyframe_timestamps.append(self.scan_header_stamp)
            self.keyframe_normals.append(self.fast_gicp.get_source_covariances())
            self.keyframe_transformations.append(self.T_corr)
        return

    def setAdaptiveParams(self):

        spaciousness = self.metric.spaciousness[-1]

        if spaciousness > 5.0:
            spaciousness = 5.0
        elif spaciousness < 0.5:
            spaciousness = 0.5
        
        self.keyframe_thresh_dist_ = spaciousness

        # density = self.metric.density[-1]

        # if density > 2.0 * self.gicp_max_corr_dist_:
        #     density = 2.0 * self.gicp_max_corr_dist_
        # elif density < 0.5 * self.gicp_max_corr_dist_:
        #     density = 0.5 * self.gicp_max_corr_dist_
        
        # self.fast_gicp.set_max_correspondence_distance(density)

    def pushSubmapIndices(self, dists, k, frames):
        # make sure dists is not empty
        if not dists:
            return

        # maintain max heap of at most k elements
        pq = queue.PriorityQueue()
        for d in dists:
            if pq.qsize() < k:
                pq.put(d)
            else:
                # we dont have access to the first element without removing it
                qd = pq.get()
                pq.put(qd)
                if pq.qsize() >= k and qd > d:
                    pq.put(d)
                    pq.get()

        # get the kth smallest element, should be at top of heap
        kth_element = pq.get()

        # get all elements smaller or equal to the kth smallest element
        for i in range(len(dists)):
            if dists[i] <= kth_element:
                self.submap_kf_idx_curr.append(frames[i])

    def buildSubmap(self, state):
        # clear vector of keyframe indices to use for submap
        self.submap_kf_idx_curr = []

        # calculate distance between current pose and poses in keyframe set
        ds = []
        keyframe_nn = []
        for i in range(self.num_processed_keyframes):
            d = np.sqrt(
                (state.p[0] - self.keyframes[i].translation[0]) ** 2
                + (state.p[1] - self.keyframes[i].translation[1]) ** 2
                + (state.p[2] - self.keyframes[i].translation[2]) ** 2
            )
            ds.append(d)
            keyframe_nn.append(i)

        # get indices for top k nearest neighbor keyframe poses
        self.pushSubmapIndices(ds, self.submap_knn_, keyframe_nn)

        # get convex hull indices
        self.computeConvexHull()

        # get distances for each keyframe on convex hull
        convex_ds = []
        for c in self.keyframe_convex:
            convex_ds.append(ds[c])

        # get indices for top kNN for convex hull
        self.pushSubmapIndices(convex_ds, self.submap_kcv_, self.keyframe_convex)

        # get concave hull indices
        self.computeConcaveHull()

        # get distances for each keyframe on concave hull
        concave_ds = []
        for c in self.keyframe_concave:
            concave_ds.append(ds[c])

        # get indices for top kNN for concave hull
        self.pushSubmapIndices(concave_ds, self.submap_kcc_, self.keyframe_concave)

        # sort current and prev submap kf list of indices
        sorted(set(self.submap_kf_idx_curr))
        sorted(self.submap_kf_idx_prev)

        # check if submap has changed from previous iteration
        if self.submap_kf_idx_curr != self.submap_kf_idx_prev:
            self.submap_hasChanged = True
            self.pauseSubmapBuildIfNeeded()

            # reinitialize submap cloud and normals
            submap_cloud = np.array([])
            submap_normals = []

            for k in self.submap_kf_idx_curr:
                # create current submap cloud
                if submap_cloud.shape[0] == 0:
                    submap_cloud = self.keyframes[k].pcd.to_array()
                else:
                    np.concatenate([submap_cloud, self.keyframes[k].pcd.to_array()])
                submap_normals += self.keyframe_normals[k]

            self.submap_cloud = pcl.PointCloud()
            self.submap_cloud.from_array(submap_cloud)
            self.submap_normals = submap_normals

            self.pauseSubmapBuildIfNeeded()

            self.fast_gicp.set_input_target(self.submap_cloud)
            self.submap_kf_idx_prev = self.submap_kf_idx_curr

    def buildKeyframesAndSubmap(self, state):

        for i in range(self.num_processed_keyframes, len(self.keyframes), 1):

            raw_keyframe = self.keyframes[i].pcd
            raw_covariances = self.keyframe_normals[i]
            T = self.keyframe_transformations[i]

            Td = np.asarray(T, dtype=np.float64)
            transformed_keyframe = transform_pointcloud(raw_keyframe, T)
            transformed_covariances = [Td @ cov @ Td.T for cov in raw_covariances]

            self.num_processed_keyframes += 1
            self.keyframes[i].pcd = transformed_keyframe
            self.keyframe_normals[i] = transformed_covariances

            # Async in dlio
            self.publishKeyframe(self.keyframes[i], self.keyframe_timestamps[i])
        self.pauseSubmapBuildIfNeeded()
        self.buildSubmap(state)

    def pauseSubmapBuildIfNeeded(self):
        """
        Pause submap if main thread needs resources.
        May not be needed here without threading
        """
        pass

    def debug(self):

        print("------------------------------------------------------------")
        print("                         DLIOpy                             ")
        print("------------------------------------------------------------")

        # Keyframes
        print(f"Current Processed Keyframes: {self.num_processed_keyframes}")

        # Spaciousness
        print(f"Current Spaciousness: {self.metric.spaciousness[-1]}")

        # Total length traversed
        length_traversed = 0.0
        p_curr = np.zeros(3)
        p_prev = np.zeros(3)

        for t in self.trajectory:

            if (p_prev == 0).all():
                p_prev = t.p
                continue

            p_curr = t.p

            l = np.sqrt(np.sum((p_curr - p_prev)**2))
            if l >= 0.1:
                length_traversed += l
                p_prev = p_curr
        
        self.length_traversed = length_traversed

        print(f"Current Length Traversed: {self.length_traversed} M")

        # Get Current RAM usage
        process = psutil.Process(os.getpid())
        memory_info = process.memory_info()

        # rss: Resident Set Size - the non-swapped physical memory that a process has used
        # vms: Virtual Memory Size - the total virtual memory used by the process
        rss_memory = memory_info.rss / (1024 * 1024)  # Convert bytes to MB
        vms_memory = memory_info.vms / (1024 * 1024)  # Convert bytes to MB

        print(f"Current RSS Memory Usage: {rss_memory:.2f} MB")
        print(f"Current VMS Memory Usage: {vms_memory:.2f} MB")

        # Computation Time
        avg_computation_time = (sum(self.comp_times) * 1000) / len(self.comp_times)
        print(f"Computation Time: {self.comp_times[-1] * 1000} ms")
        print(f"Average Computation Time: {avg_computation_time} ms")


def main(args=None):
    rclpy.init(args=args)
    node = OdomNode()
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
