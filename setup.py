from setuptools import find_packages, setup
import os
from glob import glob

package_name = 'dliopy'
param_dir = os.path.join('share', package_name, 'cfg')
launch_dir = os.path.join('share', package_name, 'launch')

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (param_dir, [os.path.join('cfg', 'params.yaml')]),
        (param_dir, [os.path.join('cfg', 'dliopy.yaml')]),
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*'))),
        (launch_dir, [os.path.join('launch', 'dliopy.rviz')])
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='pflueger-nail',
    maintainer_email='pflueger.j@northeastern.edu',
    description='Python Version of DLIO',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'dliopy_odom_node = dliopy.odom:main'
        ],
    },
)
