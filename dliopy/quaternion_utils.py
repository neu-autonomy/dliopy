import numpy as np


# =======================================
# Helper function for Quaternion Handling
# =======================================

def quaternion_conjugate(quat):
    w, x, y, z = quat
    return np.array([w, -x, -y, -z])


def quaternion_inverse(quat):
    return quaternion_conjugate(quat)/np.linalg.norm(quat)


def quaternion_multiplication(q1, q2):

    # Extracting individual components
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2

    # Calculating the product
    w = w1*w2 - x1*x2 - y1*y2 - z1*z2
    x = w1*x2 + x1*w2 + y1*z2 - z1*y2
    y = w1*y2 - x1*z2 + y1*w2 + z1*x2
    z = w1*z2 + x1*y2 - y1*x2 + z1*w2

    return np.array([w, x, y, z])


def rotate_vector_by_quaternion(vector, quat):

    quat_vector = np.array([0, *vector])
    quat_conj = quaternion_conjugate(quat)
    rotated_vector = quaternion_multiplication(quaternion_multiplication(quat, quat_vector), quat_conj)
    return rotated_vector[1:]


def quaternion_to_rotation_matrix(quat):

    w, x, y, z = quat
    xx, yy, zz = x * x, y * y, z * z
    xy, xz, yz = x * y, x * z, y * z
    wx, wy, wz = w * x, w * y, w * z

    R = np.array([[1 - 2 * (yy + zz),     2 * (xy - wz),     2 * (xz + wy)],
                  [    2 * (xy + wz), 1 - 2 * (xx + zz),     2 * (yz - wx)],
                  [    2 * (xz - wy),     2 * (yz + wx), 1 - 2 * (xx + yy)]])
    return R


def rotation_matrix_to_quaternion(R):

    q = np.zeros(4)
    trace = np.trace(R)

    if trace > 0:
        s = np.sqrt(trace + 1.0) * 2
        q[0] = 0.25 * s
        q[1] = (R[2, 1] - R[1, 2]) / s
        q[2] = (R[0, 2] - R[2, 0]) / s
        q[3] = (R[1, 0] - R[0, 1]) / s
    else:
        if R[0, 0] > R[1, 1] and R[0, 0] > R[2, 2]:
            s = np.sqrt(1.0 + R[0, 0] - R[1, 1] - R[2, 2]) * 2
            q[0] = (R[2, 1] - R[1, 2]) / s
            q[1] = 0.25 * s
            q[2] = (R[0, 1] + R[1, 0]) / s
            q[3] = (R[0, 2] + R[2, 0]) / s
        elif R[1, 1] > R[2, 2]:
            s = np.sqrt(1.0 + R[1, 1] - R[0, 0] - R[2, 2]) * 2
            q[0] = (R[0, 2] - R[2, 0]) / s
            q[1] = (R[0, 1] + R[1, 0]) / s
            q[2] = 0.25 * s
            q[3] = (R[1, 2] + R[2, 1]) / s
        else:
            s = np.sqrt(1.0 + R[2, 2] - R[0, 0] - R[1, 1]) * 2
            q[0] = (R[1, 0] - R[0, 1]) / s
            q[1] = (R[0, 2] + R[2, 0]) / s
            q[2] = (R[1, 2] + R[2, 1]) / s
            q[3] = 0.25 * s

    return q