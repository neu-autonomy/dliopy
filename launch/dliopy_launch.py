import os
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition   
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    current_pkg = FindPackageShare('dliopy')

    # Set default arguments
    rviz = LaunchConfiguration('rviz', default='false')
    pointcloud_topic = LaunchConfiguration('pointcloud_topic', default='points_raw')
    imu_topic = LaunchConfiguration('imu_topic', default='imu_raw')

    # Define arguments
    declare_rviz_arg = DeclareLaunchArgument(
        'rviz',
        default_value=rviz,
        description='Launch RViz'
    )

    declare_pointcloud_topic_arg = DeclareLaunchArgument(
        'pointcloud_topic',
        default_value=pointcloud_topic,
        description='Pointcloud topic name'
    )

    declare_imu_topic_arg = DeclareLaunchArgument(
        'imu_topic',
        default_value=imu_topic,
        description='IMU topic name'
    )

    # Load parameters
    dliopy_yaml_path = os.path.join(
        get_package_share_directory('dliopy'),  # Replace with your package name
        'cfg',
        'dliopy.yaml'
    )
    dliopy_params_yaml_path = os.path.join(
        get_package_share_directory('dliopy'),  # Replace with your package name
        'cfg',
        'params.yaml'
    )
    # dliopy_yaml_path = PathJoinSubstitution([current_pkg, 'cfg', 'dliopy.yaml'])
    # dliopy_params_yaml_path = PathJoinSubstitution([current_pkg, 'cfg', 'params.yaml'])

    # DLIO Odometry Node
    dliopy_odom_node = Node(
        package='dliopy',
        executable='dliopy_odom_node',
        output='screen',
        parameters=[dliopy_yaml_path, dliopy_params_yaml_path],
        remappings=[
            ('pointcloud', pointcloud_topic),
            ('imu', imu_topic),
            # ('descriptor', descriptor_topic),
            ('odom', 'dliopy/odom_node/odom'),
            ('pose', 'dliopy/odom_node/pose'),
            ('path', 'dliopy/odom_node/path'),
            ('kf_pose', 'dliopy/odom_node/keyframes'),
            ('kf_cloud', 'dliopy/odom_node/pointcloud/keyframe'),
            ('deskewed', 'dliopy/odom_node/pointcloud/deskewed'),
        ],
    )

    # # dliopy Mapping Node
    # dliopy_map_node = Node(
    #     package='dliopy',
    #     executable='dliopy_map_node',
    #     output='screen',
    #     parameters=[dliopy_yaml_path, dliopy_params_yaml_path],
    #     remappings=[
    #         ('keyframes', 'dliopy/odom_node/pointcloud/keyframe'),
    #     ],
    # )

    # RViz node
    # rviz_config_path = PathJoinSubstitution([current_pkg, 'launch', 'dliopy.rviz'])
    rviz_config_path = os.path.join(
        get_package_share_directory('dliopy'),  # Replace with your package name
        'launch',
        'dliopy.rviz'
    )
    rviz_node = Node(
        package='rviz2',
        executable='rviz2',
        name='dliopy_rviz',
        arguments=['-d', rviz_config_path],
        output='screen',
        condition=IfCondition(LaunchConfiguration('rviz'))
    )

    return LaunchDescription([
        declare_rviz_arg,
        declare_pointcloud_topic_arg,
        declare_imu_topic_arg,
        dliopy_odom_node,
        # dlio_map_node,
        rviz_node
    ])
